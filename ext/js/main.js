function install_loading_conf_hook() {
    window.Skype.__onConfigurationLoaded = window.Skype.onConfigurationLoaded;
    window.Skype.onConfigurationLoaded = function(conf) {
        conf.SkypeLyncWebExperience.featureFlags.consoleLogging = true;
        conf.SkypeLyncWebExperience.featureFlags.pluginlessCallingChrome = true;
        conf.SkypeLyncWebExperience.featureFlags.pluginlessCallingChromeLinux = true;
        conf.SkypeLyncWebExperience.featureFlags.errorLogReporting = true;
        return Skype.__onConfigurationLoaded(conf);
    }
}
var observer = new MutationObserver(function(mutations, observer) {
    mutations.forEach(function(mutation) {
        // when Chrome 52 is default version, use
        // for (var node of mutation.addedNodes) {
        for (var i = 0; i < mutation.addedNodes.length; ++i) {
            var node = mutation.addedNodes[i]
            if (node && node.nodeName.toLowerCase() === 'script' && node.getAttribute('src') && node.getAttribute('src').indexOf('SkypeBootstrap') !== -1) {
                node.addEventListener('load', function(evt) {
                    inject_code(install_loading_conf_hook, true)
                })
                observer.disconnect();
            }
        }
    })
});

function main() {
    chrome.extension.sendMessage("is_enabled", function(response) {
        if (response.is_enabled) {
            observer.observe(document, {
                childList: true,
                attributes: true,
                subtree: true,
            });
        }
    });
}
main();
