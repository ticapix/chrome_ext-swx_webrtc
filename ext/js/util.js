function inject_code(fct, auto_run) {
    var elt = document.createElement("script");
    elt.setAttribute("type", "text/javascript");
    elt.appendChild(document.createTextNode(fct));
    if (auto_run) {
        elt.appendChild(document.createTextNode("(" + fct.name + ")()"));
    }
    var prom = new Promise(function(resolve, reject) {
        (document.getElementsByTagName('head')[0] || document.body || document.documentElement).appendChild(elt);
        // elt.parentNode.removeChild(elt);
        resolve()
    })
    return prom;
}
