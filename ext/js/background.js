var is_enabled = true; // default value
function updateIcon(tab) {
    var path = 'res/icon32g.png'
    if (is_enabled) {
        path = 'res/icon32.png'
    }
    chrome.browserAction.setIcon({
        path: path
    });
    if (tab !== undefined) {
        chrome.tabs.reload(tab.id)
    }
}
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
    if (request === 'is_enabled') {
        sendResponse({
            is_enabled: is_enabled
        })
    }
})
chrome.storage.sync.get({
    is_enabled: is_enabled
}, function(items) {
    is_enabled = items.is_enabled;
    updateIcon();
})
chrome.browserAction.onClicked.addListener(function(tab) {
    is_enabled = !is_enabled
    chrome.storage.sync.set({
        is_enabled: is_enabled
    }, function() {
        updateIcon(tab)
    })
});