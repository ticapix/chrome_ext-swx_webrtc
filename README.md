# SWX - Chrome#

This extension is available at https://chrome.google.com/webstore/detail/skype-web-client-webrtc/bhmnniffopnnfbmfafeadpdeplhmbhhc

**This is experimental. No support whatsoever.**

### How I wrote this extension ?###

1. browse to [web.skype.com](https://web.skype.com)
2. open the developer console using F12
3. saw a request to a bootstrap script https://...skype.com/.../SkypeBootstrap...js...
4. followed by a request to a config service https://...config.skype.net/config/...?...&callback=Skype.onConfigurationLoaded
5. the above request suggests that the retrieve conf uses a onConfigurationLoaded callback on a Skype object.
6. so the first step is to detect when the bootstrap is done loading
7. install a hook on Skype.onConfigurationLoaded
8. edit the retrieve conf using flags grepped from the source of the biggest file (500KB!) fullExperience.min.js

Check the file [/ext/js/main.js](https://bitbucket.org/ticapix/chrome_ext-swx_webrtc/src/master/ext/js/main.js)